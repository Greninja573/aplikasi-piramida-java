package main;
import java.util.Scanner;
public class Piramida {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int tinggi;
		
		System.out.println("Aplikasi Pembuat Pyramid");
		System.out.print("Input Tinggi yang Di Inginkan: ");
		tinggi = scan.nextInt();
		
		for(int i=1; i<=tinggi; i++) {
			for(int j=i; j<=tinggi; j++) {
			System.out.print(" ");	
			}
			for(int k=0; k<=(i*2)-2; k++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
