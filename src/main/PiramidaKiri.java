package main;
import java.util.Scanner;
public class PiramidaKiri {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int tinggi;
		
		System.out.println("Aplikasi Piramida Kiri");
		System.out.print("Input Tinggi yang Di Inginkan: ");
		tinggi = scan.nextInt();
		
		for (int i=1; i<=tinggi; i++) {
			for(int j=1; j<=i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
