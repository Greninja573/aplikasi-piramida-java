package main;
import java.util.Scanner;
public class PiramidaKanan {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int tinggi;
		
		System.out.println("Aplikasi Piramida Kanan");
		System.out.print("Input Tinggi yang Di Inginkan: ");
		tinggi = scan.nextInt();
		
		for(int i=1; i<=tinggi; i++) {
			for(int j=4; j>=i; j--) {
				System.out.print(" ");
			}
			for(int k=1; k<=i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
