# Aplikasi Pembuat Piramida

Aplikasi ini akan membuat Piramida dengan tinggi sesuai input yang di inginkan, Ada 3 tipe piramida: Piramida biasa, Piramida Kiri, Piramida Kanan

## Cara Menggunakan

Cukup input angka yang di inginkan, aplikasi secara otomatis membuat piramida sesuai tinggi yang di input

# Development

Aplikasi ini dikembangkan dengan bahasa pemograman Java dengan IDE Eclipse

Made by Rifqi Fadhilah ([Gitlab](https://www.gitlab.com/Greninja573))